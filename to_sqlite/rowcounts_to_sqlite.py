#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import path,sep
from os import stat
import sqlite3 as db3
from sys import argv
#import logging
from datetime import date
from datetime import datetime as dt
import re
from time import sleep

from string import ascii_letters,digits,printable
#SET_LOWERUPPER_AND_DIGITS=set(''.join([ascii_lowercase,ascii_uppercase,digits]))
SET_LOWERUPPER_AND_DIGITS=set(ascii_letters).union(digits)
SET_PRINTABLE = set(printable)
#SET_PRINTABLE_NOT_PERIOD = set(printable).difference(set(chr(46)))

try:
	import MySQLdb as dbm
except ImportError:
	print("Is MySQL-python (Mysql/mariadb connector) installed?")
	# apt-get install python3-mysqldb
	# yum install MySQL-python
	exit(191)

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'

TABLECHECK_FROM='SELECT TABLE_NAME FROM information_schema.TABLES '
ROWCOUNT_TPL='SELECT count(*) FROM {0};'
SLEEPSECS = 120
TWO_TO_TWENTYTHREE = 2**23
# 2^23 is max for mediumint when signed (maria/mysql) otherwise 2^24
TWO_TO_TWENTYFOUR = 2**24

SQLITE_CREATE_TPL = ("CREATE TABLE IF NOT EXISTS"
" {0}(sampled_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,"
" rowcount integer NOT NULL, "
" rowincrease integer,"
" PRIMARY KEY(sampled_at,rowcount)"
" );")
"""CREATE UNIQUE INDEX idx_rincrease ON t1(rowincrease)"""
"""CREATE UNIQUE INDEX idx_rincrease_at ON t1(rowincrease,sampled_at)"""

dbfound_flag = False
tablefound_flag = False
date_today = date.today()
isoformat_now = dt.now().isoformat()
yyyyww = "{0}{1:0>2}".format(date_today.year,date_today.isocalendar()[1])
yyyy = ''+str(date_today.year)

mcon = None
dbcon = None


def isfile_and_positive_size(target_path_given):
	from os import path as pth
	target_path = target_path_given.strip()
	try:
		if not pth.isfile(target_path):
			return False
	except IOError as e:
		return False

	from os import stat
	try:
		if not stat(target_path).st_size:
			return False
	except IOError as e:
		return False
	return True


def connect_get_mcursor(connect_dict,ssl_arg='',verbosity=1):
	""" Connect to AN EXISTING Maria/Mysql database.
	If table does not exist then attempt to create the table.
	"""
	global mcon
	global tablefound_flag
	cur = None
	try:
		mcon = dbm.connect(**connect_dict,ssl=ssl_arg)
		cur = mcon.cursor()
	except:
		if verbosity > 0:
			print("Error connecting to maria/mysql dbname={0}.".format(dbname))

	return cur


def connect_get_cursor(database_path,verbosity=0):
	global dbcon
	cur = None
	isoformat_now = dt.now().isoformat()
	if path.exists(database_path):
		if verbosity > 0:
			print("{0} Attempting append to sqlite db at {1}.".format(
				isoformat_now,database_path))
		try:
			dbcon = db3.connect(database_path,timeout=2)
		except db3.Error as e:
			print('Sqlite connection error occurred: %s', e.args[0])
			return None
		except Exception as e:
			print('Sqlite connection error: %s', e)
			return None
		dbcon.isolation_level = None
		cur = dbcon.cursor()
	else:
		dbcon = db3.connect(database_path)
		cur = dbcon.cursor()
	return cur


def has_table_m(cursor,table_name,verbosity=0):
	tablefound_flag = False
	if table_name is not None:
		# TW is a short alias for TABLE WHERE
		TW="WHERE TABLE_NAME='{0}' AND TABLE_SCHEMA=database();".format(table_name)
		tablecheck = TABLECHECK_FROM+TW
		print(tablecheck)
		try:
			cursor.execute(tablecheck)
		except dbm.OperationalError:
			raise
		except:
			print('Unknown error during tablecheck')
			raise
		row = cursor.fetchone()
		tname = row[0]
		if verbosity > 1 and tname:
			print("InnoDB/MyISAM Database does have table named {0}".format(table_name))
		if tname:
			tablefound_flag = True
	return tablefound_flag


def create_table(cur,table_name,verbosity=0):
	""" Having a cursor object does not guarantee that the file was an sqlite
	database. A plain text file get a cursor, but fails on cur.execute() """
	global tablefound_flag

	SQLSTRING_CREATE=SQLITE_CREATE_TPL.format(table_name)

	#if debug is True:
	#	isoformat_now = dt.now().isoformat()
	#	print("{0} Creating target table in the sqlite db.".format(yyyy))

	try:
		#print(SQLSTRING_CREATE)
		cur.execute(SQLSTRING_CREATE)
		tablefound_flag = True
	except db3.ProgrammingError as e:
		err_str = "Sqlite execute() ProgrammingError occurred: {0}".format(e)
		print(err_str)
		cur.close()
		return False
	except db3.Error as e:
		err_str = "Sqlite execute() create table error {0}: {1}".format(
			database_path, e.args[0])
		print(err_str)
		cur.close()
		return False
	except:
		err_str = "Sqlite error during .execute() create if not exists."
		print(err_str)
		cur.close()
		return False
	return True


def insert_sample(table_name,rowcount,rowcount_increase=None,verbosity=0):

	global dbcon
	global tablefound_flag
	INSERT_NEXT_PREFIX='Next we insert into the rowcounting table'

	if not tablefound_flag:
		create_table(dbcon.cursor())

	if verbosity > 0:
		print("{0} rowcount={1}.".format(INSERT_NEXT_PREFIX,rowcount))

	try:
		#if cur is None:
		curinsert = dbcon.cursor()
		#print(sample_tuple[1])
		insert_list=[rowcount]
		if rowcount_increase:
			insert_list.append(rowcount_increase)
		elif 0 == rowcount_increase:
			insert_list.append(rowcount_increase)
		else:
			insert_list.append('null')
		insert_tuple=tuple(insert_list)
		insert_stmt="""INSERT INTO {0} (rowcount,rowincrease)
 VALUES(?,?);""".format(table_name)
		if verbosity > 1:
			print("next is sqlite .execute() of {0}.".format(insert_stmt))
		curinsert.execute(insert_stmt,insert_tuple)
	except db3.OperationalError as e:
		err_str = 'Sqlite execute() OperationalError occurred: {0}'.format(e)
		print(err_str)
		if curinsert:
			curinsert.close()
		return False
	except db3.ProgrammingError as e:
		err_str = 'Sqlite execute() ProgrammingError occurred: {0}.format(e)'
		print(err_str)
		if curinsert:
			curinsert.close()
		return False
	except db3.Error as e:
		err_str = 'Sqlite execute() error occurred: {0}'.format(e)
		print(err_str)
		if curinsert:
			curinsert.close()
		curinsert.close()
		return False
	except ValueError as e:
		err_str = 'Sqlite insert failed ValueError: {0}'.format(e)
		print(err_str)
		if curinsert:
			curinsert.close()
		return False
	except Exception as e:
		err_str = 'Sqlite insert failed: {0}'.format(e)
		print(err_str)
		if curinsert:
			curinsert.close()
		return False

	if curinsert is None:
		curinsert.close()
	return True


def looped_sampling(table_name,sleep_seconds=300,verbosity=0):
	global dbcon
	global mcon
	m_counter=ROWCOUNT_TPL.format(table_name)
	inserted_count = 0
	rowcount_stored = 0
	rowincrease = 0
	while rowincrease >= 0:
		cur_ro = None
		mcon.rollback()		# rollback() to close any previous mysql/maria TX
		""" When operating in an auto-commit ON environment, not committing or rollback
		would mean you are always within same transaction (so same fetch result).
		Think REPEATABLE READ.
		"""
		try:
			cur_ro = mcon.cursor()
			cur_ro.execute(m_counter)
			row_tup = cur_ro.fetchone()
			if verbosity > 1:
				print(row_tup)
			try:
				rowcount = int(row_tup[0])
			except OverflowError:
				print("rowcount too large!")
				raise
			except:
				raise
		except dbm.OperationalError:
			raise
		except:
			print('Unknown error during fetch of rowcount')
			raise
		if cur_ro:
			cur_ro.close()
		if 0 == rowcount_stored:
			rowcount_increase = 0
		else:
			try:
				rowincrease = rowcount - rowcount_stored
			except:
				print("Unexpected result rowcount-rowcount_stored!")
				raise
		inserted_flag = insert_sample(table_name,rowcount,rowincrease,verbosity)
		if inserted_flag:
			inserted_count += 1
		dbcon.commit()		# ensure the insert to sqlite is committed
		# A negative rowincrease will break out of the loop - not usually what we want
		if rowincrease < 0:
			rowincrease = 0
		# rowincrease is never used again after the insert()
		rowcount_stored = rowcount
		try:
			sleep(sleep_seconds)
		except KeyboardInterrupt:
			print("exiting loop as KeyboardInterrupt caught!")
			rowincrease = -1

	return inserted_count


if __name__ == "__main__":

	DEBUG = True

	dbname = None
	interval_mins=10
	tname = None
	if (len(argv) < 2):
		print('Expects at least 3 args. First arg is dbname. Second is interval mins')
		exit(141)

	if len(argv) > 1:
		dbname = argv[1]

		if len(argv) > 2:
			interval_mins = argv[2]

			if len(argv) > 3:
				tname = argv[3]

	if DEBUG is True:
		print("Arguments in use: db={0} ; interval_mins={1} ; table={2}".format(dbname,interval_mins,tname))
	SSL_STEM='/etc/mysql'
	mca=SSL_STEM+'/mysql_ca.pem'
	mcert=SSL_STEM+'/mysql_cert.pem'
	mkey=SSL_STEM+'/mysql_key.pem'
	ssl_dict = {'ca': mca,
		'cert': mcert,
		'key': mkey
	}
	if not isfile_and_positive_size(mca):
		exit(111)
	if not isfile_and_positive_size(mcert):
		exit(121)
	if not isfile_and_positive_size(mkey):
		exit(131)
	SSL_ARG = ssl_dict
	if dbname is None:
		exit(141)
	m4dict= { 'host': '35.35.35.35',
		'user': 'stats',
		'passwd': 'ckckck',
		'db': dbname
		}

	mcur = None
	try:
		#mcon=MySQLdb.connect(**m4dict,ssl=SSL_ARG)
		#mcon=dbm.connect(**m4dict,ssl=SSL_ARG)
		#callout to connect_get_cursor(connect_dict,ssl_arg='',verbosity=1): 
		mcur = connect_get_mcursor(m4dict,SSL_ARG,verbosity=1)
	except dbm.OperationalError:
		if 'host' in m4dict:
			print("+++ Unable to connect to mysql/mariadb on host {0}".format(m4dict['host']))
		raise
		exit(151)
	except:
		raise
		exit(161)

	tfound_count = 0
	if has_table_m(mcur,tname,2):
		tfound_count += 1

	inserted_flag = False
	cur = None
	if tfound_count > 0:
		dbpath="/var/log/{0}{1}.sqlite".format(tname,yyyy)
		cur = connect_get_cursor(dbpath)
		# Whether it has it or not, try and create table (using not exists qualifier)
		created = create_table(cur,tname,verbosity=0)
		if not created:
			exit(171)
		#inserted_flag = insert_sample(tname,0,0)
		#dbcon.commit()
	#if inserted_flag:
	try:
		interval_secs = 60*int(interval_mins)
	except:
		interval_secs = 300
	insert_verbosity=0
	inserted_count = looped_sampling(tname,interval_secs,insert_verbosity)
	print("During Looped sampling achieved {0} inserts".format(inserted_count))

	if mcon:
		mcon.close()
	if dbcon:
		dbcon.close()


