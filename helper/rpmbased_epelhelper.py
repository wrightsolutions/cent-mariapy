#!/usr/bin/env python
#   Copyright 2018 Gary Wright http://www.wrightsolutions.co.uk/contact

import subprocess
import shlex
from sys import exit

CENT7_INSTALL_EPEL="""# sudo yum install epel-release

Likely CentOS SEVEN with release files like below
::::::::::::::
/etc/centos-release
::::::::::::::
CentOS Linux release 7.5.1804 (Core) 
::::::::::::::
/etc/os-release
::::::::::::::
NAME="CentOS Linux"
VERSION="7 (Core)"
ID="centos"
ID_LIKE="rhel fedora"
VERSION_ID="7"
PRETTY_NAME="CentOS Linux 7 (Core)"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:centos:centos:7"

epel version of saltstack: 2015.5.10-2.el7  ... yum install salt-ssh salt
For 2015.5 packaging salt-call is in with the minion: yum whatprovides '*bin/salt-call'
"""


CENT6_INSTALL_EPEL="""# sudo yum install epel-release; sudo yum --enablerepo=epel install zile

Likely CentOS SIX with release files like below
::::::::::::::
/etc/system-release
::::::::::::::
CentOS release 6.9 (Final)
::::::::::::::
/etc/system-release-cpe
::::::::::::::
cpe:/o:centos:linux:6:GA

epel version of saltstack: 2015.5.10-2.el6  ... yum install salt-ssh sal
For 2015.5 packaging salt-call is in with the minion: yum whatprovides '*bin/salt-call'
"""

AL2_INSTALL_EPEL="""epel-release is available in Amazon Linux Extra topic "epel"

To use, run
# sudo amazon-linux-extras install epel

Learn more at about this proprietary fork of Centos/RedHat Linux at:
https://aws.amazon.com/amazon-linux-2/faqs/#Amazon_Linux_Extras

amzn2 tracks centos 7 epel and so saltstack: 2015.5.10-2.el7  ... yum install salt-ssh salt
For 2015.5 packaging salt-call is in with the minion: yum whatprovides '*bin/salt-call'
"""

AL1_INSTALL_EPEL="""epel-release is available to install in fashion similar to Centos6 but consult sed note below

To use, run
# sudo yum install epel-release;
Light emacs: sudo yum --enablerepo=epel install zile

Learn more at about this proprietary fork of Centos/RedHat Linux at:
https://aws.amazon.com/amazon-linux/faqs/

epel version of saltstack: 2015.5.10-2.el6  ... yum install salt-ssh salt
For 2015.5 packaging salt-call is in with the minion: yum whatprovides '*bin/salt-call'
Sed note showing sed to manually enable epel (seems to be required for amzn1)
more epel6enabled.sed
0,/gpgcheck=/{s$enabled\=0$enabled\=1$}
"""

rpm_based_flav = 'centos'	# 	default to centos

cmdp = 'rpm -qa --qf "%{NAME} %{VERSION} %{RELEASE} %{ARCH} %{PACKAGER} %{VENDOR} %{DISTRIBUTION}" binutils'
#print dfcmd  
cmdsub = subprocess.Popen(shlex.split(cmdp),bufsize=2048,
				stderr=subprocess.STDOUT,
				stdout=subprocess.PIPE)

sub_out, cmdsub_err = cmdsub.communicate()
sub_rc = cmdsub.returncode
epel_help = None
binutils_ver = 0
if 0 == sub_rc:
	for line in sub_out.split('\n'):
		if 'binutils' in line:
			line_array = line.split()
			#print(line_array)
			binutils_ver = line_array[1]
			if '.amzn2' in line and binutils_ver.startswith('2.29'):
				rpm_based_flav = 'amzn2'	# 	Amazon fork ver 2
				epel_help = AL2_INSTALL_EPEL
			elif '.amzn' in line:
				rpm_based_flav = 'amzn1'	# 	Amazon fork ver 1
				epel_help = AL1_INSTALL_EPEL
			elif binutils_ver.startswith('2.27'):
				rpm_based_flav = 'centos7'	#	SEVEN
				epel_help = CENT7_INSTALL_EPEL
			elif binutils_ver.startswith('2.20.'):
				rpm_based_flav = 'centos6'	#	SIX
				epel_help = CENT6_INSTALL_EPEL
			else:
				pass
else:
	# rpm not found or binutils not installed
	print("sub_rc={0}".format(sub_rc))
#print(binutils_ver)
print("flav: {0}".format(rpm_based_flav))

if epel_help is not None:
	print(epel_help)

exit(sub_rc)

