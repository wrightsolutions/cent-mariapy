#  MySQLdb python interface to Maria/MySQL

yum install MySQL-python

Scripts in this directory are designed to work the MySQLdb interface
that is a standard package in CentOS 7
( when last checked the version was MySQL-python-1.2.5-1.el7 )

import MySQLdb


## Extract from the package description

MySQLdb is an interface to the popular MySQL database server for Python.
The design goals are:

* Compliance with Python database API version 2.0
* Thread-safety
* Thread-friendliness (threads will not block each other)
* Compatibility with MySQL 3.23 and up

