#!/usr/bin/env python
from __future__ import print_function
import random
import string
from sys import argv

STRING_LEN=24

if __name__ == "__main__":
	#string_unreduced = string.ascii_uppercase + string.ascii_lowercase + string.digits
	string_select = 'abcdefhjkprstuvwxyz23456789'
	initial_letter = None
	suffix = None
	if len(argv) > 1:
		initial_letter = str(argv[1])
		try:
			initial_letter=initial_letter[0]
		except:
			initial_letter = None
		if len(argv) > 2:
			suffix = str(argv[2])
	generated = ''.join(random.choice(string_select) for _ in range(STRING_LEN))
	if initial_letter is not None:
		generated = initial_letter+generated[1:]
	if suffix is not None:
		suffix_stripped = suffix.strip()
		generated+=suffix_stripped
	print(generated)


#import random;import string;''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(24))

