#!/usr/bin/python26

from __future__ import print_function
#from __future__ import with_statement

#from os import path,sep
from sys import argv
#import logging
from datetime import date
from datetime import datetime as dt
import re
from time import sleep
from platform import system, release, uname

from string import ascii_letters,digits,printable
#SET_LOWERUPPER_AND_DIGITS=set(''.join([ascii_lowercase,ascii_uppercase,digits]))
SET_LOWERUPPER_AND_DIGITS=set(ascii_letters).union(digits)
SET_PRINTABLE = set(printable)
#SET_PRINTABLE_NOT_PERIOD = set(printable).difference(set(chr(46)))

try:
    import MySQLdb as dbm
except ImportError:
    print("Is MySQL-python (Mysql/mariadb connector) installed?")
    # yum install MySQL-python
    exit(191)

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'
SQLSTRING_CREATE_TEMPLATE = ("CREATE TABLE {0} ("
" id mediumint NOT NULL AUTO_INCREMENT,"
" hourmin varchar(5) NOT NULL, "
" sample_string varchar(70),"
" datetime_sampled varchar(20),"
" PRIMARY KEY(id)"
" ) ENGINE={1};")

SQLSTRING_CREATE=SQLSTRING_CREATE_TEMPLATE.format('sample','MyISAM')
TABLECHECK_FROM='SELECT TABLE_NAME FROM information_schema.TABLES '
SLEEPSECS = 120
TWO_TO_TWENTYTHREE = 2**23
# 2^23 is max for mediumint when signed (maria/mysql) otherwise 2^24
TWO_TO_TWENTYFOUR = 2**24

dbcon = None

dbfound_flag = False
tablefound_flag = False
date_today = date.today()
isoformat_now = dt.now().isoformat()
yyyyww = "{0}{1:0>2}".format(date_today.year,date_today.isocalendar()[1])


def connect_get_cursor(dbname,muser,mpass,tname=None,verbosity=1):
    """ Connect to AN EXISTING Maria/Mysql database.
    If table does not exist then attempt to create the table.
    """
    global dbcon
    global tablefound_flag
    cur = None
    try:
	dbcon = dbm.connect('db01.eseyetest.local',muser,mpass,dbname)
	cur = dbcon.cursor()
    except:
        if verbosity > 0:
            print("Error connecting to maria/mysql dbname={0}.".format(dbname))

    if tname is not None:
        # TW is a short alias for TABLE WHERE
        TW="WHERE TABLE_NAME='{0}' AND TABLE_SCHEMA=database();".format(tname)
        tablecheck = TABLECHECK_FROM+TW
        print(tablecheck)
        try:
            cur.execute(tablecheck)
        except dbm.OperationalError:
            raise
        except:
            print('Unknown error during tablecheck')
            raise
        table_name = cur.fetchone()
        if verbosity > 1 and table_name:
            print("Database {0} does have table named {1}".format(dbname,tname))
        if table_name:
            tablefound_flag = True
	else:
            tablefound_flag = False
            #print(SQLSTRING_CREATE)
            try:
                cur.execute(SQLSTRING_CREATE)
                tablefound_flag = True
            except dbm.OperationalError:
                print('Error: The user probably needs ALTER, CREATE ON sample.* ')
                raise
            except:
                print('Unknown error during table create.')
                raise

    return cur


def insert_sample(sample_tuple,cur=None):

    global tablefound_flag

    curinsert = None
    #print("Next we insert into the procmem table.")
    try:
        if cur is None:
            curinsert = dbcon.cursor()
        else:
            curinsert = cur
        hourmin = "{0}:{1}".format(sample_tuple[0],sample_tuple[1])
        curinsert.execute("""INSERT INTO sample (hourmin,
sample_string,datetime_sampled) 
VALUES(%s,%s,%s);
""",(hourmin,sample_tuple[2],sample_tuple[3]))
    except dbm.OperationalError as e:
        err_str = 'Sql execute() OperationalError occurred: {0}'.format(e)
        print(err_str)
        if cur is None:
            curinsert.close()
        return False
    except dbm.Error as e:
        err_str = 'Sql execute() error occurred: {0}'.format(e)
        print(err_str)
        if cur is None:
            curinsert.close()
        curinsert.close()
        return False
    except ValueError as e:
        err_str = 'Sql insert failed ValueError: {0}'.format(e)
        print(err_str)
	print("ValueError somewhere in {0}".format(sample_tuple))
        if cur:
            curinsert.close()
        curinsert.close()
        return False
    except Exception as e:
        err_str = 'Sql insert failed: {0}'.format(e)
        print(err_str)
	print("Exception somewhere in {0}".format(sample_tuple))
        if cur:
            curinsert.close()
        curinsert.close()
        return False

    if cur is None:
        curinsert.close()
    return True


def insert_when_matching(cur=None,verbosity=1,min_force=None):
    """ Insert an entry in the table.
    Returns an empty tuple if there was an error / problem
    The field sample_string will contain '[unmatched]' when we did not match
    
    """
    sample_tuple = ()
    dt_now = dt.now()
    dt_hour = dt_now.hour
    dt_min = dt_now.minute
    if min_force is not None:
        dt_min = min_force
    isoformat_now = dt_now.isoformat()
    isoformat_now = dt_now.strftime('%Y-%m-%d %H:%M:%S')

    hour_int = None
    min_int = None
    sample_string = '[unmatched]'
    try:
        hour_int = int(dt_hour)
        min_int = int(dt_min)
        if hour_int == min_int:
            sample_string = "Matched".format(hour_int,min_int)
        if min_force is not None:
            pass
            #sample_string = "{0} (min_force={1})".format(sample_string,min_force)
    except:
        pass
    uname_tuple = ()
    if hour_int is not None and min_int is not None:
        uname_tuple = uname()

    birthminute = None
    if len(uname_tuple) > 0:
        kplusbirth = uname_tuple[3]
        kplus_array = kplusbirth.split(':')
        if len(kplus_array) > 2:
            birthmin = kplus_array[1]
    #print(birthmin)
    # Giving up on implementing the match on birthmin (time constaints for testing)
    # but data is here and usable in variable birthmin

    sample_tuple = (hour_int,min_int,sample_string,isoformat_now)
    if 'unmatched' in sample_string:
        if verbosity > 1:
            print("Unmatched as {0}!={1}".format(dt_hour,dt_min))
    else:
        insert_sample(sample_tuple,cur)

    return sample_tuple


def loop_for_mins(cur=None,mins=60,sleepsecs=60,hour=None,min=None):
    return 


if __name__ == "__main__":

    DEBUG = True

    dbname = None
    tname = None
    mode = 'onetime'
    if (len(argv) < 2):
        exit(131)

    if len(argv) > 1:
        dbname = argv[1]

        if len(argv) > 2:
            tname = argv[2]

            if len(argv) > 3:
                mode = argv[3]

    if DEBUG is True:
        print("Arguments in use: db={0} ; table={1} ; mode={2}".format(dbname,tname,mode))
    try:
        cur = connect_get_cursor('test','myuser1','sample1pass', 'sample', 2)
    except:
        raise
        exit(141)

    tup4 = ()
    if cur:
        #tup4 = insert_when_matching(cur,verbosity=1,min_force=15)
        tup4 = insert_when_matching(cur,verbosity=1)
        try:
            cur.execute("commit;")
        except:
            pass

    if DEBUG and len(tup4) > 0:
        print("Insert on match returned {0}".format(tup4))

    if dbcon:
        cur = dbcon.cursor()
        try:
            cur.execute("commit;")
        except:
            pass
        dbcon.close()

