#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2020 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

# Example 2: cat /tmp/db.dump | python3 ./msplitmode.py -m exclude u_log x_log -p='/tmp'
# Above example says exclude u_log and x_log via a postprocessing helper
# See manifest /tmp/dump202022.txt and helper /tmp/dump202022.sed

""" Tested against Python 3.7 """

# python -m unittest discover -v

from datetime import date
from datetime import datetime as dt
import fileinput
import re
from os import getenv as osgetenv
from os import linesep
#from string import printable
from string import ascii_letters,digits,printable
from sys import argv,exit,stdin

EOL=chr(10)	# \n for end of line
""" set_printable = set(printable) """
# chr(33) is exclamation mark (!)      ; chr(34) is double quote (")     ; chr(39) is single quote (')
# chr(35) is hash / octothorpe             ; chr(58) is colon (:)            ; chr(61) is equals (=)
# chr(45) is hyphen			; chr(64) is at symbol (@)
# chr(32) is space ; chr(10) is line feed

SPACER=chr(32)
HASHER=chr(35)
COLON=chr(58)
EQUALS=chr(61)
TABBY=chr(9)
DASH=chr(45)
# Above single dash / hypen, below is double dash DDASH
DDASH="{0}{0}".format(chr(45))
ASTER=chr(42)
BANG=chr(33)
FS=chr(47)
FASTERBANG="{0}{1}{2}".format(FS,ASTER,BANG)		# /*!
RE_FASTERBANG=re.compile(FASTERBANG)
BACKTICK=chr(96)		# grave accent diacritical mark
DOTTY = chr(46)
BRACE_OPEN=chr(123)
BRACE_CLOSE=chr(125)
# Above 12x are curly {}, below 4x are rounded so ()
PARENTH_OPEN=chr(40)
PARENTH_CLOSE=chr(41)
SEMICOL=chr(59)
#RE_BRACKETED_TIME_OR_NUM = re.compile(r'\[[\-T0-9.:]*\]')
#RE_PALPHA = re.compile('%[a-z]+',re.IGNORECASE)
RE_PALPHA = re.compile('%[a-zA-Z]+')
RE_INT10 = re.compile('[0-9]{1,10}')
RE_TIME6 = re.compile('[0-9]{2}:[0-9]{2}:[0-9]{2}')
RE_DATABASE_CURRENT = re.compile("{0}\s+Current\s+Database:\s+".format(DDASH))
RE_DATABASE_USE = re.compile("USE\s+{0}.*{1}".format(BACKTICK,SEMICOL))
RE_DUMPING_DATA_TABLE = re.compile("{0}\s+Dumping\s+data\s+for\s+table".format(DDASH))
RE_PRIMARY_KEY = re.compile("PRIMARY KEY")
RE_TABLE_DROP_IF = re.compile("DROP\s+TABLE\s+IF\s+EXISTS\s+{0}".format(BACKTICK))
RE_TABLE_STRUCT = re.compile("{0}\s+Table\s+structure".format(DDASH))
RE_TABLE_CREATE = re.compile("CREATE\s+TABLE\s+{0}".format(BACKTICK))
RE_UNLOCK_TABLES = re.compile("UNLOCK TABLES{0}".format(SEMICOL))

# Next is a pattern that is internally generated (matches some lines in our manifest)
RE_TABLE_FILE = re.compile("{0}\s+table\s+file{1}".format(DDASH,COLON))

STRING_NTH_ARG_REGEX_FAILED_TEMPLATE="Arg {0} did not pass the required regex test!"

#SET_LOWERUPPER_AND_DIGITS=set(''.join([ascii_lowercase,ascii_uppercase,digits]))
SET_LOWERUPPER_AND_DIGITS=set(ascii_letters).union(digits)
SET_PRINTABLE=set(printable)
#SET_PRINTABLE_NOT_PERIOD = set(printable).difference(set(chr(46)))
TRANSLATE_DELETE = ''.join(map(chr, range(0,9)))

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3
STATE_DEPENDENT=4
STATE_UNKNOWN_MESSAGE='STATE UNKNOWN'
DESCRIPTION='Maria/Mysql dump processor / splitter'

try:
	from argparse import ArgumentParser, RawTextHelpFormatter
except ImportError:
	print(STATE_UNKNOWN_MESSAGE)
	exit(STATE_UNKNOWN)

VERSION='0.1'

"""PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)
"""

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'
date_today = date.today()
isoformat_now = dt.now().isoformat()
YYYYWW = "{0}{1:0>2}".format(date_today.year,date_today.isocalendar()[1])

re_wordchar = re.compile('\w')
re_tab_then_wordchar = re.compile("{0}\w".format(TABBY))
""" re_spaces_two_or_more = re.compile('\s\s+\w')
The regex '\s\s+\w' would probably not do what you require as
\s is equivalent to the class [ \t\n\r\f\v]
"""
re_spaces_two_or_more = re.compile(SPACER+"{2,}\w")
re_tab_then_space_then_wordchar = re.compile('\t {1,}\w')
re_space_then_tab_then_wordchar = re.compile(' {1,}\t\w')
re_hasher = re.compile(HASHER)
re_space_equals_space = re.compile("{0}{1}{0}".format(SPACER,EQUALS))
SPACENUM_DEFAULT=4



class ArgParser(ArgumentParser):
	def error(self, message):
		#self.print_help()
		print(STATE_UNKNOWN_MESSAGE, ': ', message)
		exit(STATE_UNKNOWN)


def parser_initialise():
	try:
		par = ArgParser(description=DESCRIPTION,formatter_class=RawTextHelpFormatter)
	except:
		par = None

	if par is None:
		return par

	par.add_argument('-V', '--version', help='Show plugin version',
		action='store_true')
	par.add_argument('-p', '--pathstem', help='Path stem to use when creating output',
		action='store', dest='pathstem', default='/tmp', required=False)
	par.add_argument('-l', '--linesmax', help='Maximum number of lines to process',
		action='store', dest='linesmax', type=int, default=100000)
	par.add_argument('-m', '--mode', help='Mode of operation (include or exclude)',
		action='store', dest='mode', default=None)
	par.add_argument('tables', nargs='*', help='list of tables to exclude or include (dependent on mode)')

	return par


def lines_joined(lines,joiner=chr(10)):
	""" chr(9) is tab  ; chr(10) is line feed ;
	chr(32) is space ; chr(61) is equals (=)
	"""
	joined = joiner.join(lines)
	return joined


def none_if_unprintables(unfiltered):
	#stripped = unfiltered.strip()
	if set(unfiltered).issubset(SET_PRINTABLE):
		return unfiltered
	return None


def args_valid(args_list):
	""" Helper for shlex / process type preparation. """
	pipe_count = 0
	for arg in args_list:
		try:
			if PIPEY in arg:
				pipe_count+=1
				break
		except TypeError:
			pass

	return (pipe_count < 1)


def args_valid3(args=[], argnum=0, arg_regex=None):
	feedback_line = ''
	arg=args[argnum]
	#print("Testing arg={0}".format(arg))
	valid3rc = 0
	if arg_regex.match(arg):
		pass
	else:
		# 1+argnum so report back to the user is more natural (one indexed)
		feedback_line = STRING_NTH_ARG_REGEX_FAILED_TEMPLATE.format(1+argnum)
		valid3rc = 204
	return (valid3rc,feedback_line)


def lines_forward_of(lines_given,regex_matcher,including=True):
	""" Given a list of lines, move through them from the first line
	forward and retain those that are regex_matcher onward
	Stop looking for match() against regex_matcher once have first match
	Two stage process (i) find where in the lines list 
	... and (ii) work forwards from there appending
	"""
	if lines_given is None or len(lines_given) < 1:
		raise

	lines_reversed = lines_given[:]
	lines_reversed.reverse()
	lines = []
	idx = 0
	idx_matched = None
	for line in lines_reversed:
		if regex_matcher.match(line):
			idx_matched = idx
			break
		idx += 1
	#print(idx,idx_matched,idx_matched)
	#idx_matched = len(lines_given) - idx_matched
	#print(idx,idx_matched,idx_matched)

	if idx_matched is not None and len(lines_given) > 0:
		"""
		#print(idx,idx_matched,idx_matched,len(lines_given))
		for line in lines_given[idx_matched:]:
			lines.append(line)
		"""
		lines = lines_reversed[:idx_matched]
		lines.reverse()

	return lines


def backtickless(line_given,paired_flag=True,stripped='both'):
	""" Remove backticks
	returns None if unmatched backticks when paired_flag is True or if not all backticks removed
	"""
	if stripped == 'both':
		line = line_given.strip()
	else:
		line = line_given
	bcount = line_given.count(BACKTICK)
	if paired_flag and bcount%2:
		return None
	if bcount > 0:
		# Try and take a partial of the line
		lineless = line.strip(BACKTICK)
		pos = lineless.rindex(BACKTICK)
		if pos > 0:
			# Not all backticks removed so return None
			line = None
		else:
			line = lineless
	return line


def word_ult_backtickless(line_given,paired_flag=True,stripped='right',ult_ignore=PARENTH_OPEN):
	if len(line_given) < 5:
		return None

	bcount = line_given.count(BACKTICK)
	if paired_flag and bcount%2:
		return None

	if stripped == 'both':
		line = line_given.strip()
	elif stripped == 'right':
		line = line_given.rstrip()
	else:
		line = line_given.lstrip()

	word_ult = line.split()[-1]
	if ult_ignore:
		if ult_ignore == word_ult.strip():
			# Redefine word_ult ignoring requested
			pos = line.rindex(ult_ignore)
			word_ult = line[:pos].split()[-1]
	word = word_ult.strip(BACKTICK)

	#if 'USE' in line_given:
	#	print(word,line_given)

	return word


def line_up_until_last_backtick(line_given,paired_flag=True):
	line = ''
	bcount = line_given.count(BACKTICK)
	if paired_flag and bcount%2:
		return line
	if bcount > 0:
		# Try and take a partial of the line
		pos = line_given.rindex(BACKTICK)
		if pos > 0:
			line = line_given[:pos]
	return line


def tname_from_two_inputs(tname1,tname2,tname_default=None):
	tname1stripped = tname1.strip()
	if len(tname1stripped) != len(tname2.strip()):
		return tname_default
	tname2stripped = tname2.strip()
	tname_backtickless = backtickless(tname1stripped)
	if len(tname_backtickless) < 1:
		return tname_default
	if tname_backtickless == backtickless(tname2stripped):
		tname = tname_backtickless
	return tname


def process_fileinput(fileinput_given,pathstem='/tmp',verbosity=0):
	""" Iterate over the fileinput_given
	"""

	outfilestem = "{0}{1}".format(pathstem,FS)
	datafilestem = "{0}{1}".format(pathstem,FS)
	sedfilestem = "{0}{1}".format(pathstem,FS)

	lines_manifest = []
	mline = ''
	lines = []
	lines_stored = []	# A short term buffer for replay helping
	idx = 0
	# Next index (tidx) is an index of the number of tables dealt with 
	tidx = 0
	tcreate_idx = 0
	tunlock_idx = 0
	datafile = '/tmp/dump.out'		# Pathed string of the datafile - does not contain ddl
	dataf = None				# placeholder for the actual file object of the datafile
	outfile = '/tmp/dump.out'		# Pathed string of the outfile (named ...createload)
	outf = None				# placeholder for the actual file object of the outfile
	#sedfile = '/tmp/dump.out'		# Pathed string of the post processing file
	#sedf = None				# placeholder for the actual file object for post processing
	sedfile = "{0}dump{1}{2}sed".format(sedfilestem,YYYYWW,DOTTY)
	sedf = open(sedfile,'w')
	database_used = None
	commented_use = None
	tname_stored = None
	dumping_flag = False
	for line_raw in fileinput_given:
		idx += 1
		if outf: outf.write(line_raw)
		if dumping_flag and dataf:
			# In this block we write to the datafile (does not contain ddl)
			#dataf.write("{0}{1}".format(line_raw,EOL))
			dataf.write(line_raw)
			if len(line_raw) < 20 and RE_UNLOCK_TABLES.match(line_raw):
				tunlock_idx = idx
				dataf.write(SPACER)
				dataf.flush()
				dataf.close()
				mline_pos = "{0} {1} table positions in original file:".format(HASHER,tname)
				sedder = "sed -n '{0},{1} p'".format(tcreate_idx,tunlock_idx)
				if mode == 'only':
					sed_suffix = 'p'	#	print 'p'
				elif mode == 'exclude':
					sed_suffix = 'd'	#	delete 'd'
				else:
					sed_suffix = 'zZz'
				if mode in ['exclude','only']:
					if tname in table_list_m:
						sedf.write("{0},{1} {2}{3}".format(tcreate_idx,tunlock_idx,sed_suffix,EOL))
				lines_manifest.append("{0} {1},{2} {3}".format(mline_pos,tcreate_idx,tunlock_idx,sedder))
				mline = "{0} table file: {1} for table {2}".format(DDASH,datafile,tname)
				lines_manifest.append(mline)
				print(mline)	# Next we create a helpful mv command - grep it from final manifest
				mline_mv = "{0}{0}{0} mv {1} {2}{3}sql{4}".format(HASHER,datafile,tname,DOTTY,SEMICOL)
				lines_manifest.append(mline_mv)
				dataf = None
				dumping_flag = False
				tname_stored = None
				tname = None

				# print(lines_stored) ; print(len(lines_stored))
				if len(lines_stored) > 4 and len(lines_stored) < 20:
					lines_extra = lines_forward_of(lines_stored,
								RE_TABLE_CREATE,True)
				elif len(lines_stored) > 20:
					lines_extra = lines_forward_of(lines_stored[-20:],
								RE_TABLE_CREATE,True)
				else:
					lines_extra = []
				if len(lines_extra) > 0 and commented_use:
					lines_extra.insert(0, commented_use)

				if outfile:
					line_extra_suffix = 'lines from lines_extra next'
					print("{0} suppressed add of {1} {2}.".format(outfile,len(lines_extra),line_extra_suffix))

				#for line_extra in lines_extra:
				#	outf.write(line_extra)
				# Above we have captured / processed the pre-amble before 'TABLE CREATE'
				outf.flush()
				outf.close()

				outf = None	# stops us appending to outf the pre-amble for the next table upcoming
			else:
				continue
		else:
			if len(lines_stored) > 0 and len(lines_stored) < 20:
				lines_stored.append(line_raw)
			# Not an INSERT and may or may not be captured elsewhere, so add it to lines for now
			lines.append(line_raw)
		
		if len(line_raw.strip()) < 1:
			# Lines of all blanks need no further processing
			continue
		line = line_raw.strip()
		if RE_FASTERBANG.match(line_raw):
			continue

		lstripped_len = len(line_raw.lstrip(SPACER))
		lead_spaces_count = len(line_raw)-lstripped_len
		if 0 == lead_spaces_count:
			if RE_DATABASE_USE.match(line):
				lines_manifest.append("{0} {1}".format(HASHER,line))
				database_used = word_ult_backtickless((line.rstrip()).rstrip(SEMICOL),True,'right')
			# Most things starting double dash (--) dealt with in here
			if RE_DATABASE_CURRENT.match(line):
				lines_manifest.append(line)
			elif RE_DUMPING_DATA_TABLE.match(line):
				dumping_flag = True
				tname = tname_from_two_inputs(tname_stored,tname_create,'default')
				datafile = "{0}{1}{2}{3}idx{4}".format(datafilestem,tname,DOTTY,YYYYWW,tidx)
				dataf = open(datafile,'w')
			elif RE_TABLE_STRUCT.match(line):
				lines_stored = []
				lines_stored.append(line_raw)
				word_ult = line_raw.split()[-1]
				tname_stored = word_ult.strip(BACKTICK)
				print(tname_stored,tname_stored,line_raw)
			elif RE_TABLE_CREATE.match(line):
				tcreate_idx = idx
				tname_create = word_ult_backtickless(line,True,'right',PARENTH_OPEN)
				tname = tname_from_two_inputs(tname_create,tname_stored.strip(),None)
				if tname:
					tidx += 1
					print(tidx,tidx,tidx)
					if database_used is not None:
						commented_use = "{0} USE {1}{2}{1}".format(DDASH,BACKTICK,database_used)
					print(commented_use)
					outfile = "{0}{1}{2}{3}idx{4}{2}createload".format(outfilestem,tname,DOTTY,YYYYWW,tidx)
					outf = open(outfile,'w')
					outf.write("{0}{1}".format(commented_use,EOL))	# Put an indicative USE at the top
					outf.write(line_raw)	# Put the table create in as an entry in our outf
				else:
					print("Skipping {0} line: {1}".format(tname_create,line_raw))
			elif RE_TABLE_DROP_IF.match(line):
				pass
			else:
				pass
		else:
			# Leading spaces detected so entries in here appropriate to that
			if RE_PRIMARY_KEY.match(line):
				print(line_raw)

	assert len(lines_manifest) > 0

	with open("{0}dump{1}{2}txt".format(datafilestem,YYYYWW,DOTTY),'w') as manf:
		table_count_m = 0
		for line in lines_manifest:
			if RE_TABLE_FILE.match(line):
				table_count_m += 1
			manf.write("{0}{1}".format(line,EOL))
		line_middle = 'out from a dump file of'
		line_final = "{0}{0} Split {1} tables {2} {3} lines".format(HASHER,table_count_m,line_middle,idx)
		manf.write("{0}{1}".format(line_final,EOL))
		manf.flush()

	#print(tidx,table_count_m,len(lines))
	assert tidx == table_count_m

	# Next we output to a file named ...createplus... the table creates and similar
	with open("{0}createplus{1}{2}txt".format(datafilestem,YYYYWW,DOTTY),'w') as linef:
		for line in lines:
			linef.write(line)
		linef.flush()
	
	return idx



if __name__ == "__main__":

	program_binary = argv[0].strip()
	from os import path
	program_dirname = path.dirname(program_binary)

	PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
	PYVERBOSITY = 1
	if PYVERBOSITY_STRING is None:
		pass
	else:
		try:
			PYVERBOSITY = int(PYVERBOSITY_STRING)
		except:
			pass
	#print(PYVERBOSITY)

	""" Next verbosity_global = 1 is the most likely outcome
	unless the user specifically overrides it by setting
	the environment variable PYVERBOSITY
	"""
	if PYVERBOSITY is None:
		verbosity_global = 1
	elif 0 == PYVERBOSITY:
		# 0 from env means 0 at global level
		verbosity_global = 0
	elif PYVERBOSITY > 1:
		# >1 from env means same at global level
		verbosity_global = PYVERBOSITY
	else:
		verbosity_global = 1

	parser = parser_initialise()
	if parser is None:
		print(STATE_UNKNOWN_MESSAGE)
		exit(STATE_UNKNOWN)
	""" parse_args() default method exits status code 2
	but our local override to error() ensures argument
	problems get a STATE_UNKNOWN which is likely 3 (see constants) """
	args = parser.parse_args()

	""" Have reached this far so arguments expected by program look okay """
	if args.version:
		print('Plugin version is', VERSION)
		exit(STATE_OK)

	pathstem = '/tmp'
	if args.pathstem is not None and args.pathstem.startswith(FS):
		pathstem = args.pathstem.strip()

	table_list_m = []	# m suffix short for mode - here the table list for the mode
	mode = 'only'		# 'include' is an alias for 'only'
	if args.tables and args.mode:
		if args.mode in ['include','only']:
			mode = 'only'
		else:
			mode = 'exclude'
		table_list_m = args.tables

	processed_count = process_fileinput(stdin,pathstem,1)
	if verbosity_global > 0:
		print(processed_count)


